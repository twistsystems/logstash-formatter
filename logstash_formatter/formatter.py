import traceback
import logging
import socket
import sys
import os
import threading
from datetime import datetime
try:
    import json
except ImportError:
    import simplejson as json


class LogstashFormatterBase(logging.Formatter):

    def __init__(self, message_type='Logstash', tags=None, fqdn=False, extra_fields=None):
        """
        Defines base format and helper functions
        :param fqdn: Flag if host has a full qualified domain name
        :param log_attrs: list with log attributes to be added, which each one can be both
            the attribute name or a tupple containing the attribute name and the name
            to be used by logstash `(attr_name, attr_translation)`
            The list contains all the log attributes can be found at:
            http://docs.python.org/library/logging.html#logrecord-attributes
            default: ['message', ('pathname', 'path'), ('levelname', 'level'), ('name', 'logger_name')]
        :param extra_fields: dict of additional fields
        """
        self.extra_fields = extra_fields

        self.message_type = message_type
        self.tags = tags if tags is not None else []

        if fqdn:
            self.host = socket.getfqdn()
        else:
            self.host = socket.gethostname()

    def get_log_fields(self, record):
        
        blacklist = {"args","asctime","created", "message", "levelno", "msg", "msecs", "relativeCreated", "exc_info", "exc_text", "stack_info"}
        easy_types = (str, bool, dict, float, int, list, type(None))
        
        fields = {}

        for key, value in record.__dict__.items():
            if key not in blacklist:
                if isinstance(value, easy_types):
                    fields[key] = value
                else:
                    fields[key] = repr(value)

        return fields

    @classmethod
    def format_source(cls, message_type, host, path):
        return "%s://%s/%s" % (message_type, host, path)

    @classmethod
    def format_timestamp(cls, time):
        tstamp = datetime.utcfromtimestamp(time)
        return tstamp.strftime("%Y-%m-%dT%H:%M:%S") + ".%03d" % (tstamp.microsecond / 1000) + "Z"

    @classmethod
    def format_traceback(cls, exc_info):
        return ''.join(traceback.format_exception(*exc_info)) if exc_info else ''

    @classmethod
    def serialize(cls, message):
        return json.dumps(message)


class LogstashFormatterVersion1(LogstashFormatterBase):

    def format(self, record):
        # Create message dict
        message = {
            '@timestamp': self.format_timestamp(record.created),
            '@version': '1',
            'host': self.host,
            'tags': self.tags,
            'type': self.message_type,
            '@message': record.getMessage(),
        }

        # Add exception info if present
        if record.exc_info is not None:
            message['traceback'] = self.format_traceback(record.exc_info)

        # Add log fields
        message.update(self.get_log_fields(record))

        # Add extra fields
        if self.extra_fields:
            message.update(self.extra_fields)

        return self.serialize(message)
